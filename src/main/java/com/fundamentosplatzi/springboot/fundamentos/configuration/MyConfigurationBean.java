package com.fundamentosplatzi.springboot.fundamentos.configuration;

import com.fundamentosplatzi.springboot.fundamentos.bean.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyConfigurationBean  {
    @Bean
    public Mybean beanOperation(){
       return new Mybean2Implement();
       }
    @Bean
    public MyOperation beanOperationOperation(){
        return new MyOperationImplement();
    }
    @Bean
    public MybeenWhithDependecy beanOperationOperationWithDependency(MyOperation myOperation){
        return new MyBeanWithDependencyImplement(myOperation);
    }
    @Bean
    public MenuInteractivo beanMenuOperation(){
        return new MenuInteractivoImplement();
    }
    @Bean
    public LineaDePuntos beanLineaOperation(){
        return new LineaDePuntosImplement();
    }
}
