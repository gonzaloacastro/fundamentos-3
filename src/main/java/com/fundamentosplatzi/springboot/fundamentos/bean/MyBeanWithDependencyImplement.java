package com.fundamentosplatzi.springboot.fundamentos.bean;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

public class MyBeanWithDependencyImplement implements MybeenWhithDependecy{
    Log LOGGER = LogFactory.getLog(MyBeanWithDependencyImplement.class);
    MyOperation myOperation;

    public MyBeanWithDependencyImplement(MyOperation myOperation) {
        this.myOperation = myOperation;
    }

    @Override
    public void printWithDependency() {
        LOGGER.info("Hemos ingresado al metodo printWithDependence ");
        int numero=1;
        LOGGER.debug("El Numero Enviado como parametro es " + numero);
        System.out.println(myOperation.suma(numero));

        System.out.println("Hola desde la implementacion de un bean con dependencia");
    }
}
