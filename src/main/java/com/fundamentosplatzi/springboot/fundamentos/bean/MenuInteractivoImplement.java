package com.fundamentosplatzi.springboot.fundamentos.bean;

public class MenuInteractivoImplement implements MenuInteractivo {

    @Override
    public void mostrarMenu() {
        System.out.println("Bienvenido a este menú interactivo \n");
        System.out.println("1) Agrgar Usuario \n");
        System.out.println("2) Modificar Usuario \n");
        System.out.println("3) Eliminar Usuario \n");
    }
}
