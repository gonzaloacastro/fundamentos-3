package com.fundamentosplatzi.springboot.fundamentos.bean;

import com.fundamentosplatzi.springboot.fundamentos.FundamentosApplication;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

public class LineaDePuntosImplement implements LineaDePuntos{
    Log LOGGER= LogFactory.getLog(LineaDePuntosImplement.class);
    @Override
    public void dameLinea() {
        LOGGER.info("Este metodo solo imprime una linea de puntos");
        try {
            int numero = 10;
            int f;
            float result;
            for (f = 10; f >= 0; f--) {
                System.out.println("valor" + numero + "\n");
                result = (float) (numero/f);

            }
        }catch(Exception e){
            LOGGER.error("Error al dividir por 0" + e.getMessage());
        }
        System.out.println(".....................................................");
    }
}
