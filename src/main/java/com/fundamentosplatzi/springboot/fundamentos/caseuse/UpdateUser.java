package com.fundamentosplatzi.springboot.fundamentos.caseuse;

import com.fundamentosplatzi.springboot.fundamentos.entity.User;
import com.fundamentosplatzi.springboot.fundamentos.service.UserService;
import org.springframework.stereotype.Component;

@Component
public class UpdateUser {
    public UpdateUser(UserService userService) {
        this.userService = userService;
    }

    private UserService userService;

    public User update(User newuser, Long id) {
        return userService.update(newuser, id);
    }
}
