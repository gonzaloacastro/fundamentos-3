package com.fundamentosplatzi.springboot.fundamentos;

import com.fundamentosplatzi.springboot.fundamentos.bean.*;
import com.fundamentosplatzi.springboot.fundamentos.component.ComponentDependency;
import com.fundamentosplatzi.springboot.fundamentos.entity.User;
import com.fundamentosplatzi.springboot.fundamentos.pojo.UserPojo;
import com.fundamentosplatzi.springboot.fundamentos.repository.UserRepository;
import com.fundamentosplatzi.springboot.fundamentos.service.UserService;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Sort;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class FundamentosApplication implements CommandLineRunner{

    Log LOGGER= LogFactory.getLog(FundamentosApplication.class);
    private ComponentDependency componentDependency;
    private Mybean mybean;
    private MybeenWhithDependecy mybeenWhithDependecy;
    private MyOperation myOperation;
    private MenuInteractivo menuInteractivo;
    private LineaDePuntos lineaDePuntos;
    private MyBeanWithProperties myBeanWithProperties;
    private UserPojo userPojo;
    private UserRepository userRepository;
    private UserService userService;



    public FundamentosApplication(@Qualifier("componentTwoImplement") ComponentDependency componentDependency, Mybean mybean, MybeenWhithDependecy mybeenWhithDependecy, MyOperation myOperation, MenuInteractivo menuInteractivo, LineaDePuntos lineaDePuntos, MyBeanWithProperties myBeanWithProperties, UserPojo userPojo, UserRepository userRepository, UserService userService) {
        this.componentDependency=componentDependency;
        this.mybean=mybean;
        this.mybeenWhithDependecy=mybeenWhithDependecy;
        this.myOperation=myOperation;
        this.menuInteractivo=menuInteractivo;
        this.lineaDePuntos=lineaDePuntos;
        this.myBeanWithProperties= myBeanWithProperties;
        this.userPojo=userPojo;
        this.userRepository= userRepository;
        this.userService= userService;

    }
    public static void main(String[] args) {
        SpringApplication.run(FundamentosApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        this.clasesAnteriores();
        this.saveUserInDataBase();
        this.getInformationJpqlFromUser();
        this.saveWithErrorTransaccional();
    }

    private void saveWithErrorTransaccional(){
        User test1 = new User("Test1Transactional","Test1Transactional@dominio.com",LocalDate.now());
        User test2 = new User("Test2Transactional","Test2Transactional@dominio.com",LocalDate.now());
        User test3 = new User("Test3Transactional","Test1Transactional@dominio.com",LocalDate.now());
        User test4 = new User("Test4Transactional","Test4Transactional@dominio.com",LocalDate.now());

        List<User> users = Arrays.asList(test1, test2, test3, test4);
        try {
            userService.saveTransactional(users);
        }catch(Exception e){
            LOGGER.error("Esta es una exception controlada dentro del metodo de transaccion" + e.getMessage());
        }
        userService.getAllUsers().stream()
                .forEach(user -> LOGGER.info("Este es el usuario dentro del metodo transactional" + user));

    }

    private void getInformationJpqlFromUser(){
       /* LOGGER.info("El usuario encontrado es " +
                userRepository.findByUserEmail("Julie@dominio.com")
        .orElseThrow(() -> new RuntimeException("No se encontro el usuario")));

        userRepository.findAndSort("J", "Jhon",Sort.by("id").descending())
                .stream()
                .forEach(user -> LOGGER.info("usuario con metodo sort" + user));
        userRepository.findByName("Jhon")
                .stream()
                .forEach(user -> LOGGER.info("Usuario con query method " + user));

        LOGGER.info("El usuario con query metod findByNameAndEmail es : " +
                userRepository.findByNameAndEmail("Jhon", "Jhon@dominio.com")
                        .orElseThrow(() -> new RuntimeException("No se encontro el usuario")));
        userRepository.findByNameLike("%J%")
                .stream()
                .forEach(user -> LOGGER.info("Usuario findNameLike" + user));

        userRepository.findByNameOrEmail("user10", "null")
                .stream()
                .forEach(user -> LOGGER.info("Usuario findByNameOrEmail" + user));
*/
        userRepository.findByBirthDateBetween(LocalDate.of(2021, 3, 1),LocalDate.of(2021,07, 16))
                .stream()
                .forEach(user -> LOGGER.info("Usuario con between" + user));
        userRepository.findByNameContainingOrderByIdAsc("user")
                .stream()
                .forEach(user -> LOGGER.info("Usuario con con like y ordenado" + user));

        LOGGER.info("El usuario a partir del named parametro es " +
                userRepository.getAllByBirthDateAndEmail(LocalDate.of( 2021, 03, 12),"daniela@dominio.com")
                .orElseThrow(()->
                        new RuntimeException("No se encontro el usuario a partir del named parameter")));

    }
    private void saveUserInDataBase(){
        User user1= new User("Jhon", "Jhon@dominio.com", LocalDate.of( 2021, 03, 01));
        User user2= new User("Jhon", "Julie@dominio.com", LocalDate.of( 2021, 05, 21));
        User user3= new User("Daniela", "daniela@dominio.com", LocalDate.of( 2021, 03, 12));
        User user4= new User("user4", "user4@dominio.com", LocalDate.of( 2021, 07, 16));
        User user5= new User("user5", "user5@dominio.com", LocalDate.of( 2021, 12, 13));
        User user6= new User("user6", "user6@dominio.com", LocalDate.of( 2021, 11, 14));
        User user7= new User("user7", "user7@dominio.com", LocalDate.of( 2021, 03, 18));
        User user8= new User("user8", "user8@dominio.com", LocalDate.of( 2021, 01, 15));
        User user9= new User("user9", "user9@dominio.com", LocalDate.of( 2021, 03, 21));
        User user10= new User("user10", "user10@dominio.com", LocalDate.of( 2021, 04, 15));
        User user11= new User("user11", "user11@dominio.com", LocalDate.of( 2021, 06, 11));
        User user12= new User("user12", "user12@dominio.com", LocalDate.of( 2021, 04, 16));
        List<User> list = Arrays.asList(user1,user2,user3,user4,user5,user7,user8,user9,user10,user11,user12);
        list.stream().forEach(userRepository::save);

    }

    private void    clasesAnteriores(){
        componentDependency.saludar();
        mybean.print();
        mybeenWhithDependecy.printWithDependency();
        int numero=9;
        System.out.println("El resultado de MyOperation implement es: " + myOperation.suma(numero));
        lineaDePuntos.dameLinea();
        menuInteractivo.mostrarMenu();
        lineaDePuntos.dameLinea();
        String function = myBeanWithProperties.function();
        System.out.println(userPojo.getEmail() + " con password " + userPojo.getPassword() + "Con los siguientes años" + userPojo.getAge());
        try{
            int value=10/0;
            LOGGER.debug("Mi valor es" + value);
        }catch(Exception e){
            LOGGER.error("Error al dividir por 0" + e.getStackTrace());
        }
    }
}
